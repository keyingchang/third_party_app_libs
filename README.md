﻿<h1 align="center">OpenHarmony下可以直接使用的三方组件汇总</h1> 

### 本库收集了在OpenHarmony中可以直接依赖使用的原JavaScript、TypeScript三方组件，也欢迎开发者增加三方组件提PR到列表中。
#### 1、三方组件名称：三方组件的名称
#### 2、描述：三方组件的基本功能描述
#### 3、sample地址（非必须）：样例地址

### [OpenHarmony三方组件资源汇总](https://gitee.com/openharmony-tpc/tpc_resource) : 基于OpenHarmony开发的三方组件。

## 目录

- [工具](#工具)
- [三方组件](#三方组件)
    - [文件数据](#文件数据)
        - [文件解析](#文件解析)
        - [编码解码](#编码解码)
    - [其他](#其他)

## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio

[返回目录](#目录)

## <a name="三方组件"></a>三方组件

### <a name="文件数据"></a>文件数据

#### <a name="文件解析"></a>文件解析

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [node-csv](https://github.com/adaltas/node-csv) | 可用于解析csv文件，生成csv文件 | [sample地址](https://gitee.com/openharmony-tpc/node-csv)|
| [okio ](https://gitee.com/zdy09/okio) | okio 是一个通过数据流、序列化和文件系统来优化系统输入输出流的能力的库。| [sample地址](https://gitee.com/zdy09/okio)|   
                                     
[返回目录](#目录)

#### <a name="编码解码"></a>编码解码

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [protobufjs](https://github.com/protobufjs/protobuf.js) | 主要功能是序列化和反序列化，更高效，序列化后的体积也很小，受到广大开发者的喜爱。 | [sample地址](https://gitee.com/openharmony-tpc/protobuf)|
| [commons-codec](https://github.com/apache/commons-codec)  | 包含各种格式的简单编码器和解码器，例如 Base64 和 Hexadecimal。除了这些广泛使用的编码器和解码器之外，它还维护了一组语音编码实用程序。 | [sample地址](https://gitee.com/openharmony-tpc/commons-codec)|


[返回目录](#目录)

### <a name="其他"></a>其他
|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [rxjs](https://github.com/ReactiveX/rxjs) | JavaScript 的反应式编程库 |
| [libphonenumber-js](https://github.com/catamphetamine/libphonenumber-js) | 解析、格式化和验证国际电话号码 |

[返回目录](#目录)


#### <a name="网络相关"></a>网络相关

|三方组件名称|描述|sample地址|
|:---:|:---:|:---:|
| [httpclient](https://gitee.com/zdy09/httpclient) | httpclient是OpenHarmony 里一个高效执行的HTTP客户端，使用它可使您的内容加载更快，并节省您的流量。| [sample地址](https://gitee.com/zdy09/httpclient)|
   
                                     
[返回目录](#目录)